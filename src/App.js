import React from 'react';
import AdventCalendar from './pages/AdventCalendar';
import './App.css';

function App() {
  return (
    <div className="App">
      <AdventCalendar />
    </div>
  );
}

export default App;
