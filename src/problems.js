const problems = {
    1: {
      question: 'Skapa en enkel "Hello World" applikation i ett programmeringsspråk du inte använt tidigare.',
      
      
      hint: 'Namnet "Python" kommer inte från ormen, utan från komediserien "Monty Pythons Flying Circus".',
    },
    2: {
      question: 'Implementera en algoritm för att lösa problemet med att räkna ut Fibonacci-talen.',
      
      hint: 'Kodning kan vara lika beroendeframkallande som kaffe.',
    },
    3: {
        question: 'Skriv en funktion som omvandlar text till morsekod och vice versa.',
        
        hint: 'Den första datorprogrammeraren var Ada Lovelace, och hon skrev instruktioner för Charles Babbages analytiska maskin på 1800-talet.',
      },
      4: {
        question: 'Bygg en enkel webbapplikation med hjälp av HTML, CSS och JavaScript.',
        
        
        hint: 'En av de första kända buggarna var faktiskt en riktig bugg - en mal som fastnade i Harvard Mark II-datorn 1947.',
      },
      5: {
        question: 'Skapa ett program som genererar slumpmässiga lösenord.',
       
        hint: 'Kodning hjälper till att öva tålamod, särskilt när du letar efter buggar.',
      },
      6: {
        question: 'Utveckla en enkel chattbot med hjälp av en befintlig AI-plattform eller bibliotek.',
       
        hint: 'Stack Overflow är en rik källa till programmeringsfrustration och räddning.',
      },
      7: {
        question: 'Implementera en sökalgoritm som binärsökning eller linjärsökning.',
        
        hint: 'Enligt programmeringsmyten blir du inte en riktig programmerare förrän du har skrivit ett "Hello World"-program.',
      },
      8: {
        question: 'Skriv ett program för att hämta och analysera data från en öppen API.',
        
        hint: 'Att kommentera koden är som att skriva en instruktionsmanual för framtida dig själv.',
      },
      9: {
        question: 'Bygg en "To-Do" applikation med grundläggande CRUD (Create, Read, Update, Delete) funktioner.',
       
        hint: 'Ctrl+C och Ctrl+V är programmerarens bästa vänner.',
      },
      10: {
        question: 'Skapa ett program som konverterar enheter (till exempel Celsius till Fahrenheit eller meter till kilometer).',
        
        hint: 'Regex (regular expressions) kan vara lika skrämmande som ett mysterium för vissa.',
      },
      11: {
        question: 'Implementera en enkel miniräknare med grundläggande matematiska operationer.',
        
        hint: '"404 - Sidan hittades inte" är internetets sätt att säga: "Du gjorde något fel."',
      },
      12: {
        question: 'Utmana dig själv att lösa ett problem genom att använda rekursiv programmering.',
        
        hint: 'Kodning är som att lösa en gåta, där felet är mördaren.',
      },
      13: {
        question: 'Bygg en enkel bloggplattform där användare kan skapa, redigera och radera inlägg.',
        
        hint: 'Kodare har en egen kreativ dialekt - koden.',
      },
      14: {
        question: 'Skriv ett program som sorterar en lista med siffror i stigande ordning.',
        
        
        hint: 'Att googla är den mest använda färdigheten inom programmering.',
      },
      15: {
        question: 'Utveckla en enkel spelapplikation, som exempelvis Tic-Tac-Toe eller Hangman.',
       
        hint: 'Kaffe och kodning går hand i hand som peanut butter och jelly.',
      },
      16: {
        question: 'Bygg en RESTful API och använd den för att hämta eller uppdatera data i en databas.',
        
        hint: 'Kodning kan få dig att känna som en trollkarl som skapar magi i en svart låda.',
      },
      17: {
        question: 'Skapa ett program som använder multitrådning för att utföra olika uppgifter samtidigt.',
       
        hint: 'Programmeringsspråk är som verktyg i en verktygslåda - varje har sitt syfte.',
      },
      18: {
        question: 'Implementera en enkel datastruktur, som en stapel (stack) eller kö (queue), från grunden.',
      
        hint: 'Att skriva felaktig kod är som att skriva poesi med syntaxfel.',
      },
      19: {
        question: 'Utveckla ett program som använder regelbundna uttryck för att analysera och manipulera text.',
     
        hint: '"Its not a bug, its a feature" är en vanlig försvarslinje bland utvecklare.',
      },
      20: {
        question: 'Bygg en enkel webbskrapa (web scraper) för att hämta information från en webbsida.',
    
        hint: 'Kodning är konsten att översätta mänskliga idéer till maskinspråk.',
      },
      21: {
        question: 'Skriv ett program som implementerar en grundläggande sökalgoritm för grafer, som djupet-först-sökning (DFS) eller bredden-först-sökning (BFS). ',
      
        hint: 'Att skriva kod är som att bygga ett digitalt legosystem.',
      },
      22: {
        question: 'Skapa en enkel e-handelsapplikation med produktlistning och varukorgsfunktion.',
        
        hint: 'Open source-kod är som en gemensam konstgalleri där alla kan bidra.',
      },
      23: {
        question: 'Utmana dig själv att lösa ett problem genom att använda dynamisk programmering.',
        
        hint: 'Programmerare gillar att leva farligt genom att ignorera backups.',
      },
      24: {
        question: 'Bygg en realtidsapplikation med hjälp av WebSocket-teknologi för kommunikation mellan klient och server.',
       
         
        hint: 'Den mest fruktade frasen: "Det fungerade på min dator."',
      },
  };
  
  export default problems;