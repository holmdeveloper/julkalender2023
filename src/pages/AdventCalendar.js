import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import '../CSS/AdventCalendar.css';
import problems from '../problems'; // Update the path based on your project structure
import openedDoorImage from '../luckor/lucka1.jpeg'; // Update the path to your opened door image
import closedDoorImage from '../luckor/close.jpeg';
import Logo from "../Logo/logo-calendar.png";
import Holmdev from "../Logo/holmdev.png";
import SubmissionForm from './SubmissionForm';

Modal.setAppElement('#root'); // To avoid accessibility warnings

const AdventCalendar = () => {
  const today = new Date();
  const currentDay = today.getDate();
  const [openedCount, setOpenedCount] = useState(0);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [currentProblem, setCurrentProblem] = useState(null);
  const [selectedAnswer, setSelectedAnswer] = useState(null);
  const [answeredQuestions, setAnsweredQuestions] = useState({});
  const [showWarningModal, setShowWarningModal] = useState(false);

  

  useEffect(() => {
    const openedLuckor = JSON.parse(localStorage.getItem('openedLuckor')) || [];
    setOpenedCount(Math.max(...openedLuckor, 0));

    const storedAnswers = JSON.parse(localStorage.getItem('answeredQuestions')) || {};
    setAnsweredQuestions(storedAnswers);

    const latestOpenedLucka = openedLuckor[openedLuckor.length - 1];
    if (
      latestOpenedLucka &&
      answeredQuestions[problems[latestOpenedLucka].id] &&
      answeredQuestions[problems[latestOpenedLucka].id] === problems[latestOpenedLucka].correctAnswer
    ) {
      setCurrentProblem(problems[latestOpenedLucka]);
      setModalIsOpen(true);
    }
  }, []);

  const handleDoorClick = (day) => {
    const today = new Date();
    const decemberFirst = new Date(today.getFullYear(), 11, 1); // Månader är 0-indexerade, så 11 är december
  
    if (today < decemberFirst) {
        setShowWarningModal(true);
    return;
  }
    

    if (day <= currentDay) {
      const openedLuckor = JSON.parse(localStorage.getItem('openedLuckor')) || [];
  
      // Kontrollera om någon av de tidigare luckorna inte har öppnats
      const unopenedEarlierLucka = Array.from({ length: day - 1 }, (_, index) => index + 1)
        .find((earlierDay) => !openedLuckor.includes(earlierDay));
  
      if (!unopenedEarlierLucka) {
        setCurrentProblem(problems[day]);
        setModalIsOpen(true);
        setSelectedAnswer(null);
  
        localStorage.setItem('openedLuckor', JSON.stringify([...openedLuckor, day]));
        setOpenedCount(day); // Uppdatera openedCount med det aktuella numret på luckan
      } else {
        console.log(`Lucka ${unopenedEarlierLucka} måste öppnas först.`);
      }
    }
  };
  const closeModal = () => {
    setModalIsOpen(false);
  };

  const handleAnswerClick = (answer) => {
    setSelectedAnswer(answer);

    if (currentProblem) {
      const updatedAnswers = { ...answeredQuestions, [`${currentProblem.id}_${currentDay}`]: answer };
      localStorage.setItem('answeredQuestions', JSON.stringify(updatedAnswers));
      setAnsweredQuestions(updatedAnswers);
    }
  };
  const validateAndProcessSubmission = (repositoryLink) => {
    console.log('Type of repositoryLink:', typeof repositoryLink);
    console.log('Value of repositoryLink:', repositoryLink);
  
    if (typeof repositoryLink !== 'string') {
      console.error('Invalid repository link:', repositoryLink);
      return false;
    }
  

    return true; 
  };
  const handleFormSubmit = (repositoryLink) => {
    if (currentProblem) {
  
      const isSubmissionSuccessful = validateAndProcessSubmission(repositoryLink);
  
      if (isSubmissionSuccessful) {
        const hint = problems[currentProblem.id]?.hint;
        
        console.log('Hint:', hint);
      }
    }
  

    closeModal(); 
  };
  const handleCloseWarningModal = () => {
    setShowWarningModal(false);
  };
  return (
    <div>
      <img src={Logo} alt="Logo" style={{ width: "200px" }} />
      <div className="calendar">
        {Array.from({ length: 24 }, (_, index) => index + 1).map((day) => (
          <div
            key={day}
            className={`door ${day <= openedCount ? 'unlocked' : ''}`}
            onClick={() => handleDoorClick(day)}
          >
            <div className="door-number">{day}</div>
            {day <= openedCount ? (
              <img src={openedDoorImage} alt={`Lucka ${day}`} />
            ) : (
              <img src={closedDoorImage} alt={`Stängd lucka ${day}`} />
            )}
          </div>
        ))}
      </div>
      <img src={Holmdev} alt="Holmdev.se" style={{ width: "150px" }} />
      <div>
      <Modal
      isOpen={showWarningModal}
      onRequestClose={handleCloseWarningModal}
      contentLabel="Varning Modal"
      className="custom-modal"
      overlayClassName="custom-overlay"
    >
      <div className="warning-container">
        <h2>Oops! 🎅</h2>
        <p>Du får inte öppna luckorna före den 1 december! Tomten ser dig. 🎅👀</p>
        <button onClick={handleCloseWarningModal}>Stäng</button>
      </div>
    </Modal>
    </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Problem Modal"
        className="custom-modal"
        overlayClassName="custom-overlay"
      >
        {currentProblem && (
          <>
            <div className="problem-container">
              <h2>{`Lucka ${openedCount}`}</h2>
              <p>{currentProblem.question}</p>
             
              <SubmissionForm onFormSubmit={handleFormSubmit} openedCount={openedCount} />
              <h2>Onödiga fakta om programmering!</h2>
              <p>{currentProblem.hint}</p>

              <button onClick={closeModal}>Stäng</button>
            </div>
          </>
        )}
      </Modal>
    </div>
  );
};

export default AdventCalendar;





