import React, { useState, useEffect } from 'react';

const SubmissionForm = ({ onFormSubmit, openedCount }) => {
  const [repositoryLink, setRepositoryLink] = useState('');
  const [emailAddress, setEmailAddress] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);

  useEffect(() => {
    // Kontrollera om formuläret redan har skickats in för den här luckan
    const hasFormSubmitted = localStorage.getItem(`formSubmitted_${openedCount}`) === 'true';
    if (hasFormSubmitted) {
      setIsSubmitted(true);
    }
  }, [openedCount]);

  const handleLinkChange = (event) => {
    setRepositoryLink(event.target.value);
  };

  const handleEmailChange = (event) => {
    setEmailAddress(event.target.value);
  };

  const validateEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const validateLink = (link) => {
    return link.trim().startsWith('https://');
  };

  const handleSubmit = async () => {
    if (validateEmail(emailAddress) && validateLink(repositoryLink)) {
      setIsLoading(true);
  
      const formSubmission = {
        repositoryLink: repositoryLink,
        emailAddress: emailAddress,
      };
  
      // Formspree handling URL med din e-postadress
      const formSpreeURL = 'https://formspree.io/f/xwkdzjvj'; // Byt ut med din e-postadress
  
      try {
        // Skicka formuläret till Formspree
        const response = await fetch(formSpreeURL, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(formSubmission),
        });
  
        if (response.ok) {
          // Lyckad inlämning, hantera det här om det behövs
          console.log('Formuläret har skickats in framgångsrikt.');
          setIsSubmitted(true);
  
          // Spara informationen i localStorage med hjälp av lucknummer
          localStorage.setItem(`formSubmitted_${openedCount}`, 'true');
        } else {
          // Fel vid inlämning, hantera det här om det behövs
          console.error('Fel vid inlämning av formuläret.');
        }
      } catch (error) {
        console.error('Något gick fel:', error);
      }
  
      setIsLoading(false);
      setRepositoryLink('');
      setEmailAddress('');
    }
  };

    return (
      <div className={`submission-form ${isSubmitted ? 'submitted' : ''}`}>
        {!isSubmitted ? (
          <>
            <h2>Inlämning</h2>
            <p>
              Fyll i länken till din repository nedan och skicka in din lösning via e-post.
            </p>
            <input
              type="text"
              placeholder="Länk till din repository"
              value={repositoryLink}
              onChange={handleLinkChange}
              style={{ marginBottom: '10px' }}
            />
            <br />
            <input
              type="email"
              placeholder="Din e-postadress"
              value={emailAddress}
              onChange={handleEmailChange}
            />
            <br />
            <button onClick={handleSubmit} disabled={isLoading}>
              {isLoading ? 'Skickar in...' : 'Skicka in'}
            </button>
          </>
        ) : (
          <p className="submitted-message">Du har redan skickat in formuläret.</p>
        )}
      </div>
    );
  };
  
  export default SubmissionForm;